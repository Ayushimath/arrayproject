
function find(elements,cb) {
    
    for (let i=0;i<elements.length;i++){
        if(cb(elements[i],i,elements)===true){
            return elements[i];
        }
  
    }
    
}

module.exports=find;