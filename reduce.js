
function reduce(elements,cb,startingValue) {

    //if starting value is not given then 
    //it would take elements[0] as starting value
    let total;
    let i;
    if(startingValue=== undefined){
        total=elements[0];
        i=1;
    }
    else{
        total=startingValue;
        i=0;
    }
    //while for passing every element and starting value to cb function
    while (i<elements.length){
        total=cb(total,elements[i],i,elements);
        i++;
        //console.log(total);
    }
    return total;
}

module.exports=reduce;