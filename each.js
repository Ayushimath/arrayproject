

//each returns undefined everytime it runs
function each(elements,cb) {
    
    let result=[];
    for (let i=0;i<elements.length;i++){
        //it will skip if element doesnt exist.
        if(elements[i]){
            cb(elements[i],i,elements);
        }
        
    }
}

module.exports=each;
