
//flatten function

function flatten(elements,depth1){
    
    //set defualt depth=1 if it is not passed
    let depth;
    if(depth1===undefined){
        depth=1;
    }
    else{
        depth=depth1;
    }
    const result=[];
    
   
    function checkelements(elements,depth2){
        for (let i=0;i<elements.length;i++){
            let  depth=depth2;
            if(Array.isArray(elements[i]) && depth>0){
                depth--;
                //console.log(elements[i]);
                checkelements(elements[i],depth);
            }
            else{
                //console.log("element add",elements[i]);
                result.push(elements[i]);
            }
         }
    }
    checkelements(elements,depth);
    return result;
}

module.exports=flatten;